import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    borderRadius: string

    breakpoints: {
      sm: number
      md: number
      lg: number
    }
  }
}
