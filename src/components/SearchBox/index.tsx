import React, { useState } from 'react'

import styled from 'styled-components'

const StyledInput = styled.input`
  width: 100%;
  height: 40px;
  opacity: 0.7;
  border-radius: 4px;
  background-color: #efefef;
  padding-left: 16px;
  border: none;
  box-sizing: border-box;

  &::placeholder {
    font-family: Helvetica, sans-serif;
    font-weight: 400;
    font-style: normal;
    font-size: 14px;
  }
`

type SearchBoxProps = {
  onChange?: React.ChangeEventHandler<HTMLInputElement>
}

const SearchBox = ({ onChange }: SearchBoxProps) => {
  const [value, setValue] = useState<string>('')

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.currentTarget.value)
    onChange?.(e)
  }

  return (
    <StyledInput
      onChange={handleChange}
      placeholder="Type a name..."
      value={value}
    />
  )
}

export { SearchBox }
