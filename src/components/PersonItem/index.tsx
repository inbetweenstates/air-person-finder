import React from 'react'

import styled from 'styled-components'

import defaultImg from './dummyImage.png'

const OuterContainer = styled.li`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 120px;
`
const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 96px;
`

const ProfileImage = styled.img`
  height: 100%;
  background-color: #c4c4c4;
  margin-right: 24px;
`

const DetailsContainer = styled.div`
  text-align: left;
  display: flex;
  flex-direction: column;
`

const PersonName = styled.span`
  color: #333333;
  font-family: Helvetica;
  font-size: 16px;
  font-style: normal;
  font-weight: 700;
  line-height: 24px;
  letter-spacing: -0.025em;
  text-align: left;
`

const Details = styled.p`
  color: #666666;
  font-family: Helvetica;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: -0.015em;
  text-align: left;
  margin-top: 14px;
  max-height: 65px;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`

/**
 * React-virtualized doesn't seem to support adding space between list items
 * so I'm shimming an empty div to create the illusion of padding
 */
const FakePadding = styled.div`
  min-height: 24px;
  height: 24px;
  min-width: 100%;
  background-color: #ffffff;
`

type PersonItemProps = {
  details: string
  image?: string
  name: string
  style?: object
}

const PersonItem = ({
  details,
  name,
  image = defaultImg,
  style,
}: PersonItemProps) => (
  <OuterContainer style={style}>
    <Container>
      <ProfileImage src={image} alt="" />
      <DetailsContainer>
        <PersonName>{name}</PersonName>
        <Details>{details}</Details>
      </DetailsContainer>
    </Container>
    <FakePadding />
  </OuterContainer>
)

export { PersonItem }
