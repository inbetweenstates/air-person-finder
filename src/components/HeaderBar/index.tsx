import React from 'react'

import styled from 'styled-components'

import AirLogoSVG from './Wordmark.svg'

const HeaderBarComponent = styled.header`
  display: flex;
  align-items: center;
  width: 100%;
  height: 64px;
  box-shadow: 0px -1px 0px 0px #efefef inset;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.md}px) {
    justify-content: center;
    height: 38px;
  }
`

const AirLogo = styled.img`
  width: 60px;
  height: 34px;
  margin-left: 132px;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.md}px) {
    justify-content: center;
    margin-left: 0;
  }

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    height: 21px;
    width: 45px;
  }
`

const HeaderBar = () => (
  <HeaderBarComponent>
    <AirLogo src={AirLogoSVG} alt="" />
  </HeaderBarComponent>
)

export { HeaderBar }
