import React, { useState, useMemo } from 'react'

import styled from 'styled-components'
import { AutoSizer, List } from 'react-virtualized'

import data from '../../mockData.json'
import { filterByName } from './filterByName'
import { SearchBox } from '../SearchBox'
import { PersonItem } from '../PersonItem'

const Ul = styled.ul`
  margin: 43px 0 0;
  padding: 0;
`

export type PersonResultType = typeof data[0]

const PersonFilter = () => {
  const [nameFilter, setNameFilter] = useState<string>('')
  const filteredData = useMemo(() => filterByName(nameFilter, data), [
    nameFilter,
  ])

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setNameFilter(e.currentTarget.value)

  return (
    <>
      <SearchBox onChange={handleChange} />

      <AutoSizer>
        {({ width, height }) => (
          <Ul>
            <List
              height={height}
              rowCount={filteredData.length}
              rowHeight={120}
              rowRenderer={({ index, style }) => (
                <PersonItem
                  details={filteredData[index].description}
                  image={filteredData[index].avatar}
                  name={filteredData[index].name}
                  key={filteredData[index].email}
                  style={style}
                />
              )}
              width={width}
            />
          </Ul>
        )}
      </AutoSizer>
    </>
  )
}

export { PersonFilter }
