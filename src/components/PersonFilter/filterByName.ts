import { PersonResultType } from './index'

/**
 * Really simple filter. Ideally, this would be done via fuzzy searching
 * or something.
 */
const filterByName = (name: string, arr: PersonResultType[]) =>
  name
    ? arr.filter((item) => item.name.toLowerCase().includes(name.toLowerCase()))
    : arr

export { filterByName }
