import React from 'react'
import './App.css'
import 'react-virtualized/styles.css'

import { ThemeProvider } from 'styled-components'

import { HeaderBar } from './components/HeaderBar'
import { PersonFinderPage } from './pages/PersonFinderPage'
import { AirTheme } from './styling/theme'

function App() {
  return (
    <ThemeProvider theme={AirTheme}>
      <HeaderBar />
      <PersonFinderPage />
    </ThemeProvider>
  )
}

export default App
