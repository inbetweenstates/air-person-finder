import { DefaultTheme } from 'styled-components'

const AirTheme: DefaultTheme = {
  borderRadius: '4px',

  breakpoints: {
    sm: 400,
    md: 768,
    lg: 900,
  },
}

export { AirTheme }
