import React from 'react'

import styled from 'styled-components'
import { PersonFilter } from '../../components/PersonFilter'

/**
 * Normally, I'd make a generic Page component with common styles that can be
 * reused by other pages.
 */
const Page = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  align-items: center;
  background-color: #ffffff;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.md}px) {
    padding: 0 40px;
  }

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    padding: 0 20px;
  }
`

const Container = styled.div`
  max-width: 576px;
  margin-top: 64px;
  text-align: left;
  flex: 1;
  display: flex;
  flex-direction: column;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.md}px) {
    text-align: center;
  }

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    margin-top: 0;
  }
`

const PageTitle = styled.h1`
  font-family: Georgia;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 110%;
  letter-spacing: 0.03em;
  color: #102261;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    font-size: 32px;
    line-height: 25px;
    margin: 20px 0 10px;
  }
`

const PageSubtext = styled.h3`
  font-family: Roboto;
  font-weight: 400;
  font-style: normal;
  font-size: 14px;
  line-height: 21px;
  text-align: left;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    font-size: 12px;
    line-height: 15px;
  }
`

const MainContent = styled.div`
  max-width: 576px;
  margin-top: 43px;
  margin-bottom: 75px;
  flex: 1;

  @media screen and (max-width: ${(props) => props.theme.breakpoints.sm}px) {
    margin-top: 13px;
  }
`

const PersonFinderPage = () => (
  <Page>
    <Container>
      <PageTitle>The Person Finder</PageTitle>

      <PageSubtext>
        If you just can’t find someone and need to know what they look like,
        you’ve come to the right place! Just type the name of the person you are
        looking for below into the search box!
      </PageSubtext>

      <MainContent>
        <PersonFilter />
      </MainContent>
    </Container>
  </Page>
)

export { PersonFinderPage }
