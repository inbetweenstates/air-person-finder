# Main focus:

- Code structure and cleanliness
- Mock data list was very long and scrolling was choppy even on my decent laptop
- Focused heavily on performance and smoothness
- Used react-virtualized to render a more performant list
  - This is cheating a little because the full list is available
  - Normally pagination is involved

# Things left to do:

- Accessibility on search input
- Testing
  - Would've used @testing-library/react for unit tests
  - Cypress for integration tests
- Animations
  - Would've used framer-motion
